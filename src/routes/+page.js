// since there's no dynamic data here, we can prerender

import jsyaml from "js-yaml";

export const ssr = false;

// it so that it gets served as a static asset in production
export const prerender = true;

const data = `
cv: 
    personal:
        firstName: Robert Michael
        lastName: Forster
        address:
            - 28 Back Lane
            - Dunston
            - Lincs
            - LN4 2EH
            - GB
        phone: 07771 715308
        email: robert.forster@beamvex.co.uk
        position: Senior Technical Architect
    skills:
        - Java:
            experience: 23 Years
        - JavaScript:
            experience: 5 Years
        - TypeScript:
            experience: 3 Years
        - Oracle:
            experience: 10 Years
        - Lambda Functions:
            experience: 7 Years    
        - 
`;

/** @type {import('./$types').PageLoad} */
export function load({ params }) {
  const pagedata = jsyaml.load(data);
  console.log(JSON.stringify(pagedata, null, 2));

  return {
    pagedata,
  };
}
